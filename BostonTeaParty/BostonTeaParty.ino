#include "TimerOne.h"

#include <avr/sleep.h>
#include <avr/power.h>

#define VERSION			0.2

#define POWER_SAVING	0

#define NUM_DISPLAYS 	(4)

#define MS_PER_MIN		(60000)
#define MS_PER_SEC		(1000)

const byte numbers[11] = {
	0b11111100,	//0
	0b01100000,	//1
	0b11011010,	//2
	0b11110010,	//3
	0b01100110,	//4
	0b10110110,	//5
	0b10111110,	//6
	0b11100000,	//7
	0b11111110,	//8
	0b11100110,	//9
	0b00000000	//off
};

const byte dash = 0b00000010;

const byte letter_o = 0b00111010;
const byte letter_n = 0b00101010;
const byte letter_e = 0b11011110;
const byte letter_t = 0b00011110;

const byte letter_D = 0b11111100;
const byte letter_O = 0b11111100;
const byte letter_S = 0b10110110;


#define a_pin 3
#define b_pin A0
#define c_pin 7
#define d_pin 8
#define e_pin 9
#define f_pin 4
#define g_pin 5
#define DP_pin 6

#define a_bit 7
#define b_bit 6
#define c_bit 5
#define d_bit 4
#define e_bit 3
#define f_bit 2
#define g_bit 1
#define DP_bit 0

#define zero_pin 10
#define one_pin 11
#define two_pin 12
#define three_pin 13

//Needs to be an interrupt pin
#define button_pin 2

#define vibrator_pin A4
#define notifier_pin A5

#define COMMON_ANODE_off	
#define COMMON_CATHODE

#define BIT_IS_SET(val, bit)  ((val & (1 << bit) ? true : false))
#define BIT_IS_CLR(val, bit)  ((val & (1 << bit) ? false : true))

#ifdef COMMON_ANODE
	#define SHOW_SEGMENT(val, bit) BIT_IS_CLR(val, bit)
	#define DISPLAY_ON	true
	#define DISPLAY_OFF	false
#elif defined(COMMON_CATHODE)
	#define SHOW_SEGMENT(val, bit) BIT_IS_SET(val, bit)
	#define DISPLAY_ON	false
	#define DISPLAY_OFF	true
#else
	#error "No pin mode define"
#endif

const byte display_enable_pins[NUM_DISPLAYS] = {
	zero_pin,one_pin,two_pin,three_pin
};

volatile int display_index_isr = 0;
volatile bool shadow_changed = false;

volatile byte display_buffer_isr[NUM_DISPLAYS] = {
	0,0,0,0
}; //<saved as a bit code

byte display_shadow_buffer[NUM_DISPLAYS] = {
	0,0,0,0
}; //<saved as a bit code

enum tStateMachine{STATE_SLEEP, STATE_SETUP, STATE_COUNTING, STATE_DONE};

tStateMachine state;

#define START_TIME_ms			(2*60000)
#define INCREMENT_ms			(30000)
#define MAX_TIME_ms				(4*60000)

#define SELECT_TIMEOUT_ms		(3000)

#define BUTTON_DEBOUNCE_ms 		(100)
#define DISPLAY_MESSAGE_WAIT_ms (500)

#define CHECK_TIME_CHANGE_ms	(50)

#define SCAN_TIME_us			(1000)

#define NOTIFY_TIME_ms			(100)

#define ALERT_MAX_TIMES			(5)

static volatile bool wake_from_sleep = false;

void setup() 
{
	power_adc_disable();
	power_spi_disable();
	power_twi_disable();

  	for (int i = 0; i < NUM_DISPLAYS; i++)
  	{
    	pinMode(display_enable_pins[i], OUTPUT);
    	digitalWrite(display_enable_pins[i], DISPLAY_OFF);
	}
  
	pinMode(a_pin, OUTPUT);
	pinMode(b_pin, OUTPUT);
	pinMode(c_pin, OUTPUT);
	pinMode(d_pin, OUTPUT);
	pinMode(e_pin, OUTPUT);
	pinMode(f_pin, OUTPUT);
	pinMode(g_pin, OUTPUT);
	pinMode(DP_pin, OUTPUT);

	pinMode(vibrator_pin, OUTPUT);
	pinMode(notifier_pin, OUTPUT);

	digitalWrite(a_pin, !DISPLAY_OFF);
	digitalWrite(b_pin, !DISPLAY_OFF);
	digitalWrite(c_pin, !DISPLAY_OFF);
	digitalWrite(d_pin, !DISPLAY_OFF);
	digitalWrite(e_pin, !DISPLAY_OFF);
	digitalWrite(f_pin, !DISPLAY_OFF);
	digitalWrite(g_pin, !DISPLAY_OFF);
	digitalWrite(DP_pin, !DISPLAY_OFF);

	digitalWrite(vibrator_pin, false);
	digitalWrite(notifier_pin, false);

	pinMode(button_pin, INPUT_PULLUP);

	Timer1.initialize(SCAN_TIME_us);
	// 
	display_number_on_seven_segment(8888);

  	Timer1.attachInterrupt(display_tick);

  	delay(DISPLAY_MESSAGE_WAIT_ms);

  	display_On_message();

  	delay(DISPLAY_MESSAGE_WAIT_ms*3);

}

void loop()
{
	static long countdown_ms = 0;
	static long start_timer_tick = 0;
	static long select_timeout = 0;

	switch (state) {
	    case STATE_SLEEP:
	    	display_blank();
	    	#if POWER_SAVING
	    	if(go_to_sleep())
	    	#else
	    	if(button_pressed())
    		#endif
	    	{
	    		countdown_ms = START_TIME_ms;
	    		select_timeout = millis();
	    		state = STATE_SETUP;

	    		display_Set_message();
	    		delay(DISPLAY_MESSAGE_WAIT_ms);
	    		display_blank();
	    		delay(DISPLAY_MESSAGE_WAIT_ms);
	    		display_Set_message();
	    		delay(DISPLAY_MESSAGE_WAIT_ms);
	    	}	
      	break;
	    case STATE_SETUP:
	    	if(button_pressed())
	    	{
	    		countdown_ms += INCREMENT_ms;
	    		if(countdown_ms > MAX_TIME_ms)
	    		{
	    			countdown_ms = START_TIME_ms;
	    		}

	    		select_timeout = millis();
	    	}

	    	format_millis_to_mins_and_display(countdown_ms);

	    	if((millis() - select_timeout) > SELECT_TIMEOUT_ms)
	    	{
	    		display_blank();
	    		delay(DISPLAY_MESSAGE_WAIT_ms);
    			format_millis_to_mins_and_display(countdown_ms);
    			delay(DISPLAY_MESSAGE_WAIT_ms);
	    		display_blank();
	    		delay(DISPLAY_MESSAGE_WAIT_ms);
	    		format_millis_to_mins_and_display(countdown_ms);
				// delay(1000);
				// Look. Techinically this means the counter counts for +1 second.

	    		state = STATE_COUNTING;
	    		start_timer_tick = millis();
	    	}
      	break;
	    case STATE_COUNTING:
	    {
	    	long diff = millis() - start_timer_tick;
	    	long time_remaining = ((countdown_ms - diff) < 0 ? 0 : countdown_ms - diff);

	    	format_millis_to_mins_and_display(time_remaining);

	    	if(time_remaining <= 0 || button_pressed())
	    	{
	    		state = STATE_DONE;
	    	}
	    	else
	    	{
	    		delay(CHECK_TIME_CHANGE_ms);
	    	}
		}
      	break;
	    case STATE_DONE:
	    	display_Done_message();

	    	digitalWrite(notifier_pin, true);
	    	delay(NOTIFY_TIME_ms);
	    	digitalWrite(notifier_pin, false);

		    for(int i = 0; i < ALERT_MAX_TIMES; i++)
		    {
		    	display_Done_message();
		    	//Add buzzer on here
		    	digitalWrite(vibrator_pin, true);
		    	delay(DISPLAY_MESSAGE_WAIT_ms);
		    	digitalWrite(vibrator_pin, false);
		    	//Add buzzer off here	
		    	display_blank();
		    	delay(DISPLAY_MESSAGE_WAIT_ms);
		    }
	    	state = STATE_SLEEP;
      	break;
	    default:
      	break;
	}
}

void display_tick()
{
	digitalWrite(display_enable_pins[display_index_isr], DISPLAY_OFF);

	display_index_isr++;
	
	if(display_index_isr >= NUM_DISPLAYS)
	{
		display_index_isr = 0;

		if(shadow_changed)
		{
			memcpy((void*)display_buffer_isr, (void*)display_shadow_buffer, NUM_DISPLAYS);
			shadow_changed = false;
		}
	}

	digitalWrite(a_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], a_bit));
	digitalWrite(b_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], b_bit));
	digitalWrite(c_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], c_bit));
	digitalWrite(d_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], d_bit));
	digitalWrite(e_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], e_bit));
	digitalWrite(f_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], f_bit));
	digitalWrite(g_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], g_bit));
	digitalWrite(DP_pin, SHOW_SEGMENT(display_buffer_isr[display_index_isr], DP_bit));
	
	digitalWrite(display_enable_pins[display_index_isr], DISPLAY_ON);
}

void format_millis_to_mins_and_display(long milli)
{
	int mins = milli / MS_PER_MIN;
	int seconds = (milli % MS_PER_MIN) / MS_PER_SEC;

	//Displays in the format mm.ss
	display_shadow_buffer[3] = numbers[get_number_for_base_ten(seconds,1)];
	display_shadow_buffer[2] = numbers[get_number_for_base_ten(seconds, 10)];
	display_shadow_buffer[1] = numbers[get_number_for_base_ten(mins, 1)] | (1 << DP_bit);
	
	if(mins > 9)
	{
		display_shadow_buffer[0] = numbers[get_number_for_base_ten(mins, 10)];
	}
	else
	{
		display_shadow_buffer[0] = numbers[10]; //Show blank!!!
	}

	shadow_changed = true;
}

void display_number_on_seven_segment(int num)
{
	display_shadow_buffer[3] = numbers[get_number_for_base_ten(num, 1)];
	display_shadow_buffer[2] = numbers[get_number_for_base_ten(num, 10)];
	display_shadow_buffer[1] = numbers[get_number_for_base_ten(num, 100)];
	display_shadow_buffer[0] = numbers[get_number_for_base_ten(num, 1000)];

	shadow_changed = true;
}

void display_dashes()
{
	display_shadow_buffer[0] = dash;
	display_shadow_buffer[1] = dash;
	display_shadow_buffer[2] = dash;
	display_shadow_buffer[3] = dash;	

	shadow_changed = true;
}

void display_blank()
{
	display_shadow_buffer[0] = numbers[10];
	display_shadow_buffer[1] = numbers[10];
	display_shadow_buffer[2] = numbers[10];
	display_shadow_buffer[3] = numbers[10];	

	shadow_changed = true;
}

void display_Done_message()
{
	display_shadow_buffer[0] = letter_D;
	display_shadow_buffer[1] = letter_o;
	display_shadow_buffer[2] = letter_n;
	display_shadow_buffer[3] = letter_e;	

	shadow_changed = true;
}

void display_On_message()
{
	display_shadow_buffer[0] = 0x00;
	display_shadow_buffer[1] = letter_O;
	display_shadow_buffer[2] = letter_n;
	display_shadow_buffer[3] = 0x00;	

	shadow_changed = true;
}

void display_Set_message()
{
	display_shadow_buffer[0] = letter_S;
	display_shadow_buffer[1] = letter_e;
	display_shadow_buffer[2] = letter_t;
	display_shadow_buffer[3] = 0x00;	

	shadow_changed = true;
}

byte get_number_for_base_ten(int input, int power_ten)
{
	return((input/(power_ten))%10);
}

bool button_pressed()
{
	if(digitalRead(button_pin) == 0)
	{
		delay(BUTTON_DEBOUNCE_ms);
		if(digitalRead(button_pin) == 0)
		{
			while(digitalRead(button_pin) == 0) {};
			return true;
		}
		return false;
	}
	return false;
}


bool go_to_sleep()
{
	//Turn off display
	// Timer1.stop();

  	for (int i = 0; i < NUM_DISPLAYS; i++)
  	{
    	pinMode(display_enable_pins[i], INPUT);
	}
  
	pinMode(a_pin, INPUT);
	pinMode(b_pin, INPUT);
	pinMode(c_pin, INPUT);
	pinMode(d_pin, INPUT);
	pinMode(e_pin, INPUT);
	pinMode(f_pin, INPUT);
	pinMode(g_pin, INPUT);
	pinMode(DP_pin, INPUT);

	memset((void*)display_shadow_buffer, 0x00, NUM_DISPLAYS);
	memset((void*)display_buffer_isr, 0x00, NUM_DISPLAYS);

	// power_timer0_disable();
	// power_timer1_disable();
	// power_timer2_disable();

	// do
	// {
		wake_from_sleep = false;
		while(wake_from_sleep == false)
		{
			sleep_enable();
			attachInterrupt(0, button_wake_ISR, LOW);
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			cli();
			sleep_bod_disable();
			sei();
			sleep_cpu();
			/* wake up here */
			sleep_disable();
			detachInterrupt(0);
		}
	// }while(!button_pressed());
	//if button is pressed long enough to leave sleep
	
	// power_timer0_enable();
	// power_timer1_enable();
	// power_timer2_enable();


	for (int i = 0; i < NUM_DISPLAYS; i++)
  	{
    	pinMode(display_enable_pins[i], OUTPUT);
    	digitalWrite(display_enable_pins[i], DISPLAY_OFF);
	}
  
	pinMode(a_pin, OUTPUT);
	pinMode(b_pin, OUTPUT);
	pinMode(c_pin, OUTPUT);
	pinMode(d_pin, OUTPUT);
	pinMode(e_pin, OUTPUT);
	pinMode(f_pin, OUTPUT);
	pinMode(g_pin, OUTPUT);
	pinMode(DP_pin, OUTPUT);

	digitalWrite(a_pin, !DISPLAY_OFF);
	digitalWrite(b_pin, !DISPLAY_OFF);
	digitalWrite(c_pin, !DISPLAY_OFF);
	digitalWrite(d_pin, !DISPLAY_OFF);
	digitalWrite(e_pin, !DISPLAY_OFF);
	digitalWrite(f_pin, !DISPLAY_OFF);
	digitalWrite(g_pin, !DISPLAY_OFF);
	digitalWrite(DP_pin, !DISPLAY_OFF);

	memset((void*)display_shadow_buffer, 0x00, NUM_DISPLAYS);
	memset((void*)display_buffer_isr, 0x00, NUM_DISPLAYS);
	
	shadow_changed = false;

	Timer1.initialize(SCAN_TIME_us);
	Timer1.start();

	return true;
}

void button_wake_ISR()
{
	nop_loop();
	
	if(digitalRead(button_pin) == 0)
	{
		sleep_disable();
		wake_from_sleep = true;
	}
}

void nop_loop()
{
	int i = 1000;
	while(--i > 0)
	{
		__asm__("nop\n\t");
	}
}
